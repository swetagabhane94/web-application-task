import React, { Component } from "react";
import axios from "axios";
import UserData from "./UserData";
import Loader from "./Loader";

export default class FrontPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      failedAPI: false,
      fetchedData: false,
    };
  }

  fetchData = () => {
    axios
      .get("https://reqres.in/api/users?page=1")
      .then((response) => {
        this.setState({
          products: response.data,
          fetchedData: true,
        });
      })
      .catch((error) => {
        this.setState = {
          fetchData: true,
          failedAPI: true,
        };
      });
  };

  render() {
    return (
      <div>
        <div>
          <nav className="header">
           <div className="header-logo">
            <i className="fa-sharp fa-solid fa-users"></i>
            <h1>UsersDetail</h1>
            </div>
            <button  onClick={this.fetchData}> Get Users </button>
          </nav>
        </div>
        
        <div className="productDetail">
          {this.state.fetchedData ? (
            this.state.products.data.map((product) => {
              return (
                <div key={product.id}>
                  <UserData
                    imgLink={product.avatar}
                    firstName={product.first_name}
                    lasttName={product.last_name}
                    email={product.email}
                  />
                </div>
              );
            })
          ) : this.state.failedAPI ? (
            "OOPS! Sorry data not fetch"
          ) : (
            <Loader />
          )}
        </div>
      </div>
    );
  }
}
