import React from "react";

export default function Loader() {
  return (
    <div className="center">
      <div className="loader"></div>
    </div>
  );
}
