import React, { Component } from "react";

export default class UserData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="cardBox">
        <img className="card-img" src={this.props.imgLink} alt="" />
        <div className="card-body">
          <p className="name">
            {this.props.firstName} {this.props.lasttName}
          </p>
         <p className="email">Email: {this.props.email}</p>
        </div>
      </div>
    );
  }
}
