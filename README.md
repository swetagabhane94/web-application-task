# Web Application Task
## About The Task
Create a web application using create-react-app:

1. Create a User card grid layout having navbar showing any brand name
2. Add a button in the navbar saying 'Get Users', which makes an API call to get the user data
3. Use https://reqres.in/api/users?page=1 to get the data
4. Show a loader while the API fetches the data.
5. Bonus points for using custom CSS/SASS/styled-components.
6. Bonus points for clean code.

You can design your own UI for this, make it simple.
## Method Used

1. For API fetching I used npm package axios. Axios method I take the data.
2. Data iteration Array.prototype.map() method used.
3. Error handling condition used for user end. If API failed or data not fetched.
4. Custom CSS used for UI.
## Live Demo

Visit Website : https://web-application-task.vercel.app/
